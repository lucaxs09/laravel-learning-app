<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->id();

            $table->longText('mensaje');

            // Relaciones
            // usuario que hizo el comentario
            $table->foreignId('user_id')->constrained('users');

            // Post sobre el que se comentó
            $table->foreignId('post_id')->constrained('posts');


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
};
