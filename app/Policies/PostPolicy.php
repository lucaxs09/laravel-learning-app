<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        // Solo puede ver si es root o Administrador
        return $user->isRoot() or $user->isAdmin();
    }

    public function index(User $user): bool
    {

        return true;
    }

    public function view(?User $user, Post $post): bool
    {
        # segun el tipo de usuario
        $is_moderator = ($user?->isRoot() or $user?->isAdmin());
        // NOTA: cuando trabajamos con PERMISOS, no usamos ROLES para tomar decisiones.
        $is_moderator = $user->can('post.show');


        $is_autor = $post->user_id === $user?->id;

        if($is_moderator or $is_autor){
            return  true;
        }

        # le permitimos ver solo si el Post esta publicado
        return $post->isPublished();
    }

    public function create(User $user): bool
    {
    }

    public function update(User $user, Post $post): bool
    {
    }

    public function delete(User $user, Post $post): bool
    {
    }

    public function restore(User $user, Post $post): bool
    {
    }

    public function forceDelete(User $user, Post $post): bool
    {
    }
}
