<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $table = 'posts';

    # Los atributos que se pueden modificar/asignar de forma masiva

    protected $fillable = [
        'titulo','resumen',
        'body','categoria',

    ];

    protected $casts = [

    ];


    # Relaciones
    public function autor(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function comentarios(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Comentario::class,'post_id');
    }




    public function scopePublicos(Builder $builder)
    {
        return $builder->where('estado','=',2);
    }

    public function scopeOcultos(Builder $builder)
    {
        return $builder->where('estado','=',1);
    }


    public function isPublished():bool
    {
        return $this->estado === 3;

    }
}
