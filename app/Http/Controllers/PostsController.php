<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;

/**
 * @group Posts
 */
class PostsController extends Controller
{
    /**
     * Listar posts
     *
     * Esta ruta de vuelve la lista de los posts.....
     *
     * @queryParam per_page int Cantidad de items a devolver por página. Valor por defecto 10.
     */
    public function index(Request $request)
    {
        $this->authorize('index',Post::class);
        $request->validate([
            'per_page' => 'int',
            'page'     => 'int',

            'estado' => 'int',
            'search' => 'string',
        ]);
        $prePage = $request->get('per_page',10);


        # Aqui debemos crear un queryBuilder bien flexible
        $query = Post::query();

        if($request->has('estado')){
            // aplicamos el filtro por estado
            $estado = $request->get('estado');
            $query->where('estado','=',$estado);
        }

        if($request->has('search')){
            // hacemos la búsqueda
            $text= $request->get('search');

            $query->where('titulo','LIKE',"%$text%");
        }

        $query->orderBy('created_at','desc');




        // obtenemos los resultados
        $resultadoPaginado = $query->paginate($prePage);
        // siempre devuelvo un Resource
        return PostResource::collection($resultadoPaginado);
    }

    /**
     * Registrar nuevo post
     */
    public function store(Request $request)
    {
        $datos = $request->validate([

        ]);
        Post::create($datos);
    }

    /**
     * Obtener un post
     */
    public function show(Post $post)
    {
        $this->authorize('view',$post);

        return PostResource::make($post);
    }

    /**
     * Actualizar un post
     */
    public function update(Request $request, Post $post)
    {
        $this->authorize('update',$post);

    }

    /**
     * Eliminar un post
     */
    public function destroy(Post $post)
    {
        try {
            $post->delete();
            return apiful()->deleted();
        } catch (\Exception $e) {

            \Log::warning("Fallo al  eliminar un post",[
                'error'=> $e->getMessage()
            ]);

            return apiful()
                ->withStatusCode(409)
                ->withMessage("No fué posible eliminar el post");
        }
    }
}
